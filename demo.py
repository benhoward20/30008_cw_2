from finite_difference_solver import *
from scipy.integrate import quad, trapz

def u_I(x, L):
	# initial temperature distribution
	u_i = np.sin(pi*x/L)
	return u_i
	
def u_exact(x, t, L, kappa):
	# the exact solution
	u_e = np.exp(-kappa*(pi**2/L**2)*t)*np.sin(pi*x/L)
	return u_e
	
def calc_error_t(kappa, L, T, mx, mt, bc, bc_t, discretisation, exact=solve_fin_diff):
	'''
	calc_error_t:
		- Function to calculate the error in a scheme for varying mt.
		
	Inputs:
		kappa - float - The diffusion constant.
		L - float - Length of the spacial domain.
		T - float - Total time to solve for.
		mx - int - Number of mesh points in space.
		mt - int - Number of mesh points in time.
		bc - list - Boundary conditions in space, length two.
		bc_t - list - Boundary condition types, length two, 0 denotes Dirichlet and 1 denoted Neumann.
		discretisation - string - Discretisation scheme ('fe'-Forward Euler, 'be'-Backward Euler, 'cn'-Crank-Nicolson).
		exact - function - (Optional) Defines the exact solution of the PDE.
		
	Outputs:
		errors - ndarray - Differences between the integrals of u_T and u_e for each mt.
	'''
	errors = []
	vars = []
	if exact == solve_fin_diff:
		parameters = lambda n, x: [u_I, kappa, L, T, mx, 2*(mt**n), bc, bc_t, discretisation]
	else:
		parameters = lambda n, x: [x, T, L, kappa]
		
	for n in range(3,15):
		t = np.linspace(0, T, (mt**n)+1)
		x = np.linspace(0, L, mx+1)

		deltat = t[1] - t[0]
		deltax = x[1] - x[0]

		u = solve_fin_diff(u_I, kappa, L, T, mx, mt**n, bc, bc_t, discretisation)
		u_int = trapz(u, dx=deltax)
		
		u_e = exact(*tuple(parameters(n, x)))
		ue_int = trapz(u_e, dx=deltax)

		err = np.linalg.norm(ue_int - u_int)
		errors.append(err)
		
		vars.append(deltat)
	
	# plot the truncation error
	pl.loglog(vars, errors, 'r-',label='num')
	pl.xlabel(r'$\Delta$t', fontsize=14)
	pl.ylabel('Error', fontsize=14)
	pl.title('Error of Crank-Nicolson Scheme with Varying $mt$ Compared to Exact Solution.', fontsize=14, wrap=True)
	pl.show()

	return errors

def calc_error_x(kappa, L, T, mx, mt, bc, bc_t, discretisation, exact=solve_fin_diff):
	'''
	calc_error_x:
		- Function to calculate the error in a scheme for varying mx.
		
	Inputs:
		kappa - float - The diffusion constant.
		L - float - Length of the spacial domain.
		T - float - Total time to solve for.
		mx - int - Number of mesh points in space.
		mt - int - Number of mesh points in time.
		bc - list - Boundary conditions in space, length two.
		bc_t - list - Boundary condition types, length two, 0 denotes Dirichlet and 1 denoted Neumann.
		discretisation - string - Discretisation scheme ('fe'-Forward Euler, 'be'-Backward Euler, 'cn'-Crank-Nicolson).
		exact - function - (Optional) Defines the exact solution of the PDE.
		
	Outputs:
		errors - ndarray - Differences between the integrals of u_T and u_e for each mx.
	'''
	errors = []
	vars = []
	if exact == solve_fin_diff:
		parameters = lambda n, x: [u_I, kappa, L, T, 2*(mx**n), mt, bc, bc_t, discretisation]
		delta = lambda x: (x[1] - x[0])/2
	else:
		parameters = lambda n, x: [x, T, L, kappa]
		delta = lambda x: x[1] - x[0]
		
	for n in range(3,15):
		t = np.linspace(0, T, mt+1)
		x = np.linspace(0, L, (mx**n)+1)

		deltax = x[1] - x[0]

		u = solve_fin_diff(u_I, kappa, L, T, mx**n, mt, bc, bc_t, discretisation)
		u_int = trapz(u, dx=deltax)
		
		u_e = exact(*tuple(parameters(n, x)))
		ue_int = trapz(u_e, dx=delta(x))

		err = np.linalg.norm(ue_int - u_int)
		errors.append(err)
		
		vars.append(deltax)
	
	# plot the truncation error
	pl.loglog(vars, errors, 'r-',label='num')
	pl.xlabel(r'$\Delta$x', fontsize=14)
	pl.ylabel('Error', fontsize=14)
	pl.title('Error of Backwards Euler Scheme with Varying $mx$ Compared to Solution with $2mx$.', fontsize=14, wrap=True)
	pl.show()

	return errors
	
if __name__ == '__main__':

	kappa=1; L=1; T=0.5; mx=100; mt=1000; bc=[0,0]; bc_t=[0,0]; discretisation='be'

	u = solve_fin_diff(u_I, 			# function handle defining initial temp. dist.
	kappa,								# diffusion constant
	L,									# length of spatial domain
	T, 									# total time to solve for
	mx, 								# number of gridpoints in space
	mt, 								# number of gridpoints in time
	bc,									# dirichlet boundary conditions
	bc_t,								# vector specifying condition type
	discretisation,						# discretisation scheme (forward_euler, backward_euler, crank_nicholson)
	plot=True,							# plot graph
	exact=u_exact)						# function handle defining the exact solution
	print(u)
	

	mx=2; mt=1000;
	err = calc_error_x(kappa, L, T, mx, mt, bc, bc_t, discretisation) # function is slow
	print(err)

	mx=1000; mt=2; discretisation='cn'
	err = calc_error_t(kappa, L, T, mx, mt, bc, bc_t, discretisation, exact=u_exact) # function is slow
	print(err)