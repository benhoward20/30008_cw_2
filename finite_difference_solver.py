import numpy as np
import pylab as pl
from math import pi
from scipy.sparse import diags, linalg
import sys

def disc_scheme(u_j, lmbda, mx, mt, bc, bc_t, deltat, deltax, A, bc_vec, discretisation, B=None):
	'''
	disc_scheme:
		- Function to calculate the numeric solution of the PDE at time T.
		
	Inputs:
		u_j - ndarray - Initial value of u for each spacial gridpoint.
		lmbda - float - Mesh Fourier nummber.
		mx - int - Number of mesh points in space.
		mt - int - Number of mesh points in time.
		bc - list - Boundary conditions in space, length two.
		bc_t - list - Boundary condition types, length two, 0 denotes Dirichlet and 1 denoted Neumann.
		deltat - float - Grid spacing in time.
		deltax - float - Grid spacing in space.
		A - ndarray - Matrix used to solve for u_jp1.
		bc_vec - ndarray - Vector contaning boundary conditions to add to u_j.
		discretisation - string - Discretisation scheme.
		B - ndarray - (Optional) Matrix used in Crank-Nicolson to solve for u_jp1.
		
	Outputs:
		u_j - Numeric solution of the PDE at time T.
	'''
	u_jp1 = np.zeros(u_j.size)      # u at next time step
	if bc_t == [0,0]:
		# Solve the PDE: loop over all time points
		for n in range(1, mt+1):
			bc_vec[0] = lmbda * bc[0](n*deltat); bc_vec[-1] = lmbda * bc[1](n*deltat)
			u_jp1[1:-1] = update_u(A, u_j[1:-1], bc_vec, discretisation, B) # Timestep at inner mesh points
			u_jp1[0] = bc[0](n*deltat); u_jp1[mx] = bc[1](n*deltat) # Boundary conditions
			u_j[:] = u_jp1[:] # Update u_j
	elif bc_t == [0,1]:
		# Solve the PDE: loop over all time points
		for n in range(1, mt+1):
			bc_vec[0] = lmbda * bc[0](n*deltat); bc_vec[-1] = 2 * lmbda * deltax * bc[1](n*deltat)
			u_jp1[1:] = update_u(A, u_j[1:], bc_vec, discretisation, B) # Timestep at inner mesh points
			u_jp1[0] = bc[0](n*deltat) # Boundary condition
			u_j[:] = u_jp1[:] # Update u_j
	elif bc_t == [1,0]:
		# Solve the PDE: loop over all time points
		for n in range(1, mt+1):
			bc_vec[0] = -2 * lmbda * deltax * bc[0](n*deltat); bc_vec[-1] = lmbda * bc[1](n*deltat)
			u_jp1[:-1] = update_u(A, u_j[:-1], bc_vec, discretisation, B) # Timestep at inner mesh points
			u_jp1[mx] = bc[1](n*deltat) # Boundary condition
			u_j[:] = u_jp1[:] # Update u_j
	else:
		# Solve the PDE: loop over all time points
		for n in range(1, mt+1):
			bc_vec[0] = -2 * lmbda * deltax * bc[0](n*deltat); bc_vec[-1] = 2 * lmbda * deltax * bc[1](n*deltat)
			u_jp1 = update_u(A, u_j, bc_vec, discretisation, B) # Timestep at inner mesh points
			u_j[:] = u_jp1[:] # Update u_j
	return u_j

def matrix_init(mx, diag_mul, sub_mul, bc_t):
	'''
	matrix_init:
		- Function to calculate the matrix and vector of boundary conditions necessary to update u_j at each time-step.
		
	Inputs:
		mx - int - Number of mesh points in space.
		diag_mul - float - Value of each element on the diagonal of A.
		sub_mul - float - Value of each element on the sub- and super-diagonals of A.
		bc_t - list - Boundary condition types, length two, 0 denotes Dirichlet and 1 denoted Neumann.
	Outputs:
		A - ndarray - Matrix used to solve for u_jp1.
		bc_vec - ndarray - Vector contaning boundary conditions to add to u_j.
	'''
	if bc_t == [0, 0]:
		size = mx - 1
		factor = [1, 1]
	elif bc_t == [1, 1]:
		size = mx + 1
		factor = [2, 2]
	else:
		size = mx
		if bc_t[0] == 1:
			factor = [2, 1]
		else:
			factor = [1, 2]
	
	diag = diag_mul * np.ones(size)
	sub_diag = sub_mul * np.ones(size - 1)
	sup_diag = np.copy(sub_diag)
	sub_diag[-1] = sub_diag[-1] * factor[1]
	sup_diag[0] = sup_diag[0] * factor[0]
	
	A = diags([sup_diag, diag, sub_diag], offsets=[1, 0, -1], format="csc")
	
	bc_vec = np.zeros(size)
	
	return A, bc_vec

def update_u(A, u_j, bc_vec, discretisation, B):
	'''
	update_u:
		- Function to update the solution of the PDE from time t to time t+1.
		
	Inputs:
		A - ndarray - Matrix used to solve for u_jp1.
		u_j - ndarray - Numeric solution of the PDE at time t.
		bc_vec - ndarray - Vector contaning boundary conditions to add to u_j.
		discretisation - string - Discretisation scheme.
		B - ndarray - Matrix used in Crank-Nicolson to solve for u_jp1.
		
	Outputs:
		u_jp1 - Numeric solution of the PDE at time t+1.
	'''
	if discretisation == 'fe':
		u_jp1 = A.dot(u_j) + bc_vec
	elif discretisation == 'be':
		u_jp1 = linalg.spsolve(A, u_j + bc_vec)
	else:
		u_jp1 = linalg.spsolve(A, B.dot(u_j) + bc_vec)
		
	return u_jp1
		
def solve_fin_diff(u_I, kappa, L, T, mx, mt, bc, bc_t, discretisation='cn', plot=False, exact=None):
	'''
	solve_fin_diff:
		- Wrapper function to initialise the calculation of the numeric solution of the PDE at time T.
		
	Inputs:
		u_I - function - Defines the initial temperature distribution of the PDE.
			Inputs:
				x - ndarray - Linspace of spacial positions.
				L - float - Length of the spacial domain.
			Outputs:
				u_i - ndarray - Initial solution of PDE for specified space domain.
		kappa - float - The diffusion constant.
		L - float - Length of the spacial domain.
		T - float - Total time to solve for.
		mx - int - Number of mesh points in space.
		mt - int - Number of mesh points in time.
		bc - list - Boundary conditions in space, length two.
		bc_t - list - Boundary condition types, length two, 0 denotes Dirichlet and 1 denoted Neumann.
		discretisation - string - (Optional) Discretisation scheme ('fe'-Forward Euler, 'be'-Backward Euler, 'cn'-Crank-Nicolson).
		plot - Boolean - (Optional) Whether to plot a graph of the solution.
		exact - function - (Optional) Defines the exact solution of the PDE.
			Inputs:
				x - ndarray - Linspace of spacial positions.
				t - float - Time at which to evaluate the solution.
				L - float - Length of the spacial domain.
				kappa - The diffusion constant.
			Outputs:
				u_e - ndarray - Exact solution of PDE at specified time.
		
	Outputs:
		u_T - Numeric solution of the PDE at time T.
	'''
	if mx < 3:
		sys.stderr.write('Specified value of mx is too small.\n')
		return 1
	if mt < 1:
		sys.stderr.write('Specified value of mt is too small.\n')
		return 1
	if (bc_t[0] != 0 and bc_t[0] != 1) or (bc_t[1] != 0 and bc_t[1] != 1):
		sys.stderr.write('Invalid boundary condition type specified. Check bc_t.\n')
		return 1
	if discretisation not in ('fe', 'be', 'cn'):
		sys.stderr.write('Invalid discretisation scheme specified.\n')
		return 1
	# set up the numerical environment variables
	x = np.linspace(0, L, mx+1)     # mesh points in space
	t = np.linspace(0, T, mt+1)     # mesh points in time
	deltax = x[1] - x[0]            # gridspacing in x
	deltat = t[1] - t[0]            # gridspacing in t
	lmbda = kappa*deltat/(deltax**2)    # mesh fourier number		+ve for FE, -ve for BE
	
	if discretisation == 'fe' and lmbda > 0.5:
		sys.stderr.write('Forward Euler scheme will not converge for specified input values.\n')
		return 1

	# Set initial condition
	u_j = u_I(x, L)
	
	# Defining BCs as functions
	if isinstance(bc[0], int) or isinstance(bc[0], float):
		bc_0 = bc[0]
		bc[0] = lambda t : bc_0
	if isinstance(bc[1], int) or isinstance(bc[1], float):
		bc_1 = bc[1]
		bc[1] = lambda t : bc_1
	
	# Defining matrices for each discretisation method.
	if discretisation == 'fe':
		diag_mul = 1 - 2 * lmbda
		sub_mul = lmbda
		B = None
	elif discretisation == 'be':
		diag_mul = 1 + 2 * lmbda
		sub_mul = -lmbda
		B = None
	else:
		diag_mul = 1 + lmbda
		sub_mul = -lmbda / 2
		B, bc_vec = matrix_init(mx, 1 - lmbda, -sub_mul, bc_t)
		
	A, bc_vec = matrix_init(mx, diag_mul, sub_mul, bc_t)
	
	u_T = disc_scheme(u_j, lmbda, mx, mt, bc, bc_t, deltat, deltax, A, bc_vec, discretisation, B)
	
	if plot == True:
		# plot the final result and exact solution
		x = np.linspace(0, L, mx+1)
		pl.plot(x, u_T, 'ro', label='num')
		if exact != None:
			xx = np.linspace(0, L, 250)
			pl.plot(xx, exact(xx, T, L, kappa), 'b-', label='exact')
		pl.xlabel('x')
		pl.ylabel('u(x,0.5)')
		pl.legend(loc='upper right')
		pl.show()
		pass
	
	return u_T