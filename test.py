from finite_difference_solver import *
from demo import u_I, u_exact

def non_convergence(parameters):
	if solve_fin_diff(*parameters, mx=40, mt=1000, bc=[0,0], bc_t=[0,0], discretisation='fe') == 1:
		sys.stderr.flush()
		print("Test for non-convergence passed.")
	else:
		print("Test for non-convergence failed.")
	return None
	
def small_mx(parameters):
	if solve_fin_diff(*parameters, mx=2, mt=1000, bc=[0,0], bc_t=[0,0]) == 1:
		sys.stderr.flush()
		print("Test for small mx value passed.")
	else:
		print("Test for small mx value failed.")
	return None
	
def small_mt(parameters):
	if solve_fin_diff(*parameters, mx=40, mt=0, bc=[0,0], bc_t=[0,0]) == 1:
		sys.stderr.flush()
		print("Test for small mt value passed.")
	else:
		print("Test for small mt value failed.")
	return None
	
def bc_type(parameters):
	if solve_fin_diff(*parameters, mx=40, mt=1000, bc=[0,0], bc_t=[2,1]) == 1:
		sys.stderr.flush()
		print("Test for invalid bounday condition type passed.")
	else:
		print("Test for invalid bounday condition type failed.")
	return None
	
def disc_type(parameters):
	if solve_fin_diff(*parameters, mx=40, mt=1000, bc=[0,0], bc_t=[0,0], discretisation='bf') == 1:
		sys.stderr.flush()
		print("Test for invalid discretisation type passed.")
	else:
		print("Test for invalid discretisation type failed.")
	return None

def output_fe(u_I, kappa, L, T):
	u = solve_fin_diff(u_I, kappa, L, T, mx=10, mt=1000, bc=[0,0], bc_t=[0,0], discretisation='fe')
	
	x = np.linspace(0, L, 10+1)
	u_e = u_exact(x, T, L, kappa)
	
	test_list = []
	for i, j in zip(u, u_e):
		output = np.isclose(i, j, atol=1e-3)
		test_list.append(output)
	test = np.array(test_list)
	if test.all() == True:
		print("Output of Forward Euler scheme passed test.")
	else:
		print("Output of Forward Euler scheme failed test.")
	return None
	
def output_be(u_I, kappa, L, T):
	u = solve_fin_diff(u_I, kappa, L, T, mx=40, mt=1000, bc=[0,0], bc_t=[0,0], discretisation='be')
	
	x = np.linspace(0, L, 40+1)
	u_e = u_exact(x, T, L, kappa)
	
	test_list = []
	for i, j in zip(u, u_e):
		output = np.isclose(i, j, atol=1e-3)
		test_list.append(output)
	test = np.array(test_list)
	if test.all() == True:
		print("Output of Backward Euler scheme passed test.")
	else:
		print("Output of Backward Euler scheme failed test.")
	return None
	
def output_cn(u_I, kappa, L, T):
	u = solve_fin_diff(u_I, kappa, L, T, mx=40, mt=1000, bc=[0,0], bc_t=[0,0])
	
	x = np.linspace(0, L, 40+1)
	u_e = u_exact(x, T, L, kappa)
	
	test_list = []
	for i, j in zip(u, u_e):
		output = np.isclose(i, j, atol=1e-3)
		test_list.append(output)
	test = np.array(test_list)
	if test.all() == True:
		print("Output of Crank-Nicolson scheme passed test.")
	else:
		print("Output of Crank-Nicolson scheme failed test.")
	return None
	
if __name__ == '__main__':
	kappa=1; L=1; T=0.5
	parameters = (u_I, kappa, L, T)
	
	non_convergence(parameters)
	small_mx(parameters)
	small_mt(parameters)
	bc_type(parameters)
	disc_type(parameters)
	
	output_fe(u_I, kappa, L, T)
	output_be(u_I, kappa, L, T)
	output_cn(u_I, kappa, L, T)